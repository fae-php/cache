<?php

/**
 * FAE 
 */

namespace FAE\cache;

interface cache_adapter_interface
{
  function __construct(string $namespace = 'FAE\\', int $lifetime = 0, float $version = null);
}
