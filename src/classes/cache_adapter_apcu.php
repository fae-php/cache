<?php

/**
 * FAE 
 */

namespace FAE\cache;

use Symfony\Component\Cache\Adapter\ApcuAdapter;

class cache_adapter_apcu extends ApcuAdapter implements cache_adapter_interface
{

  function __construct(string $namespace = 'FAE', int $lifetime = 0, ?float $version = null)
  {
    if (!extension_loaded('apcu')) {
      throw new \RuntimeException('APCu module not loaded, cannot initialise cache');
    }
    parent::__construct($namespace, $lifetime, $version);
  }
}
