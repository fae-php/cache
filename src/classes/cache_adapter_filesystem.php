<?php

/**
 * FAE 
 */

namespace FAE\cache;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class cache_adapter_filesystem extends FilesystemAdapter implements cache_adapter_interface
{

  function __construct(string $namespace = 'FAE', int $lifetime = 0, ?float $version = null)
  {
    parent::__construct($namespace, $lifetime, APPROOT.'/cache/');
  }
}
