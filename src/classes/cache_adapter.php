<?php

/**
 * FAE 
 */

namespace FAE\cache;

use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\Simple\AbstractCache;

class cache_adapter
{

  const HIGH = 1;
  const LOW  = 0;

  public $low = [
    'apcu',
  ];
  public $high = [
    'filesystem',
  ];

  var $cacheInstance;
  var $error;

  function __construct(int $level = self::HIGH, string $namespace = null, int $lifetime = 0, float $version = null)
  {
    global $config;
    if (is_null($namespace) && $config->namespace) {
      $namespace = $config->namespace;
    }
    if ($level === self::LOW) {
      $caches = $this->low;
    } else {
      $caches = $this->high;
    }
    foreach ($caches as $cacheType) {
      $class = '\\FAE\\cache\\cache_adapter_' . $cacheType;
      try {
        $this->cacheInstance = new $class($namespace, $lifetime, $version);
        break;
      } catch (\Exception $e) {
        $this->error = $e;
      }
    }
    if (!$this->cacheInstance) {
      throw $this->error;
    }
  }

  public function getCache(): AbstractAdapter
  {
    return $this->cacheInstance;
  }
}
